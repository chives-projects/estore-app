import loginPage from './components/login-page.vue'
import cashierPage from './components/cashier-page/cashier-page.vue'
import shopkeeperPage from './components/shopkeeper-page/shopkeeper-page.vue'

import supplierManager from './components/shopkeeper-page/supplier-manager.vue'
import goodsManager from './components/shopkeeper-page/goods-manager.vue'
import cashierManager from './components/shopkeeper-page/cashier-manager.vue'
import transactionManager from './components/shopkeeper-page/transaction-manager.vue'

export const routes = [
  { path: '/login-page', alias: '/', component: loginPage },
  { path: '/cashier-page', component: cashierPage },
  { path: '/shopkeeper-page', component: shopkeeperPage,
    children: [
      { path: 'supplier-manager', components: { manager: supplierManager } },
      { path: 'goods-manager', components: { manager: goodsManager } },
      { path: 'cashier-manager', components: { manager: cashierManager } },
      { path: 'transaction-manager', components: { manager: transactionManager } },
    ]
  },
]
